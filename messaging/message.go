package messaging

type Message struct {
	Id   int      `json:"-"`
	From string   `json:"message_from"`
	To   []string `json:"message_to"`
	Subj string   `json:"message_subject"`
	Body string   `json:"message_body"`
}
