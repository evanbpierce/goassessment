package messaging

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func Connect() *sql.DB {
	connStr := "user=evan password=evan dbname=coin_ninja sslmode=disable"

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal("POSTRESQL CONNECT ERROR:", err)
	}

	return db
}

func Save(db *sql.DB, m *Message) {
	b, err := json.Marshal(m)
	if err != nil {
		log.Fatal("JSON MARSHAL ERROR:", err)
	}

	if m.Id > 0 {
		r, err := db.Exec(`UPDATE message SET message = $2 WHERE id = $1`, m.Id, fmt.Sprintf("%s", b))
		if err != nil {
			log.Fatal("POSTGRESQL UPDATE ERROR:", err)
		}

		ra, _ := r.RowsAffected()

		if ra == 0 {
			m.Id = 0
		}
	}

	if m.Id == 0 {
		err = db.QueryRow(`INSERT INTO message (message) VALUES ($1) RETURNING id`, fmt.Sprintf("%s", b)).Scan(&m.Id)
		if err != nil {
			log.Fatal("DATABASE INSERT ERROR:", err)
		}
	}
}

func Get(db *sql.DB, getid int) Message {
	var (
		err error
		id  int
		msg []byte
		m   Message
	)

	row := db.QueryRow(`SELECT * FROM message WHERE id = $1`, getid)
	err = row.Scan(&id, &msg)

	switch err {
	case sql.ErrNoRows:
	case nil:
		err = json.Unmarshal(msg, &m)
		if err != nil {
			log.Fatal("UNMARSHAL ERROR:", err)
		}

		m.Id = id
	default:
		log.Fatal("POSTGRESQL SCAN ERROR:", err)
	}

	return m
}

func Index(db *sql.DB) []Message {
	var (
		id  int
		msg []byte
		ms  []Message
	)

	rows, err := db.Query(`SELECT * FROM message ORDER BY id`)
	if err != nil {
		log.Fatal("DATABASE SELECT ALL ERROR:", err)
	}

	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&id, &msg)
		if err != nil {
			log.Fatal("POSTGRESQL SCAN ERROR:", err)
		}

		var m Message
		err = json.Unmarshal(msg, &m)
		if err != nil {
			log.Fatal("UNMARSHAL ERROR:", err)
		}

		m.Id = id

		ms = append(ms, m)
	}

	return ms
}

func Delete(db *sql.DB, id int) int64 {
	r, err := db.Exec(`DELETE FROM message WHERE id = $1`, id)
	if err != nil {
		log.Fatal("POSTGRESQL DELETE ERROR:", err)
	}

	ra, _ := r.RowsAffected()

	return ra
}

func Disconnect(db *sql.DB) {
	err := db.Close()
	if err != nil {
		log.Fatal("POSTGRESQL DISCONNECT ERROR:", err)
	}
}
