package messaging

// ----------------------------------------------------------------------------

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

// ----------------------------------------------------------------------------

func TestSave(t *testing.T) {
	assert := assert.New(t)
	db, m1, m2 := before(assert)

	b, err := json.Marshal(m1)
	assert.Equal(nil, err, "JSON Marshal error")

	bs := fmt.Sprintf("%s", b)
	assert.Contains(bs, `"message_from":`, "Missing message_from in json")
	assert.Contains(bs, `"message_to":`, "Missing message_to in json")
	assert.Contains(bs, `"message_subject":`, "Missing message_subject in json")
	assert.Contains(bs, `"message_body":`, "Missing message_body in json")

	assert.Equal(1, m1.Id, "Id value should be 1")
	assert.Equal(2, m2.Id, "Id value should be 2")

	m1.From = "Bob T Builder"
	Save(db, &m1)

	m1g := Get(db, m1.Id)

	assert.Equal("Bob T Builder", m1g.From, "Fetched From does not match Bob T Builder")

	assert.Equal(m1.Id, m1g.Id, "Fetched Id does not match original")
	assert.Equal(m1.From, m1g.From, "Fetched From does not match original")
	assert.Equal(m1.To, m1g.To, "Fetched To does not match original")
	assert.Equal(m1.Subj, m1g.Subj, "Fetched Subj does not match original")
	assert.Equal(m1.Body, m1g.Body, "Fetched Body does not match original")

	m3 := Message{Id: 41, From: "EBPierce", To: []string{"Kendra Pierce"}, Subj: "This is to my daughter", Body: "Hey there Kendra!"}

	Save(db, &m3)

	assert.Equal(m2.Id+1, m3.Id, "Fetched Id does not match expected value of 3")

	m3g := Get(db, m3.Id)

	assert.Equal(m3.Id, m3g.Id, "Fetched Id does not match original")
	assert.Equal(m3.From, m3g.From, "Fetched From does not match original")
	assert.Equal(m3.To, m3g.To, "Fetched To does not match original")
	assert.Equal(m3.Subj, m3g.Subj, "Fetched Subj does not match original")
	assert.Equal(m3.Body, m3g.Body, "Fetched Body does not match original")

	Disconnect(db)
}

func TestGet(t *testing.T) {
	assert := assert.New(t)
	db, m1, m2 := before(assert)

	m1g := Get(db, m1.Id)

	assert.Equal(m1.Id, m1g.Id, "Fetched Id does not match original")
	assert.Equal(m1.From, m1g.From, "Fetched From does not match original")
	assert.Equal(m1.To, m1g.To, "Fetched To does not match original")
	assert.Equal(m1.Subj, m1g.Subj, "Fetched Subj does not match original")
	assert.Equal(m1.Body, m1g.Body, "Fetched Body does not match original")

	m2g := Get(db, m2.Id)

	assert.Equal(m2.Id, m2g.Id, "Fetched Id does not match original")
	assert.Equal(m2.From, m2g.From, "Fetched From does not match original")
	assert.Equal(m2.To, m2g.To, "Fetched To does not match original")
	assert.Equal(m2.Subj, m2g.Subj, "Fetched Subj does not match original")
	assert.Equal(m2.Body, m2g.Body, "Fetched Body does not match original")

	Disconnect(db)
}

func TestIndex(t *testing.T) {
	assert := assert.New(t)
	db, m1, m2 := before(assert)

	ms := Index(db)

	assert.Equal(2, len(ms), "Length of data returned by Index should be 2")

	m1i := ms[0]
	m2i := ms[1]

	assert.Equal(m1.Id, m1i.Id, "Fetched Id does not match original")
	assert.Equal(m1.From, m1i.From, "Fetched From does not match original")
	assert.Equal(m1.To, m1i.To, "Fetched To does not match original")
	assert.Equal(m1.Subj, m1i.Subj, "Fetched Subj does not match original")
	assert.Equal(m1.Body, m1i.Body, "Fetched Body does not match original")

	assert.Equal(m2.Id, m2i.Id, "Fetched Id does not match original")
	assert.Equal(m2.From, m2i.From, "Fetched From does not match original")
	assert.Equal(m2.To, m2i.To, "Fetched To does not match original")
	assert.Equal(m2.Subj, m2i.Subj, "Fetched Subj does not match original")
	assert.Equal(m2.Body, m2i.Body, "Fetched Body does not match original")

	Disconnect(db)
}

func TestDelete(t *testing.T) {
	assert := assert.New(t)
	db, m1, m2 := before(assert)

	assert.Equal(int64(1), Delete(db, m1.Id), "Should have deleted row 1 only")
	assert.Equal(int64(1), Delete(db, m2.Id), "Should have deleted row 2 only")
	assert.Equal(int64(0), Delete(db, m1.Id), "Should have deleted no rows")
	assert.Equal(int64(0), Delete(db, m2.Id), "Should have deleted no rows")

	ms := Index(db)

	assert.Equal(0, len(ms), "All rows should have been deleted")

	Disconnect(db)
}

// ----------------------------------------------------------------------------

func before(assert *assert.Assertions) (*sql.DB, Message, Message) {
	db := resetData(assert)
	m1, m2 := addData(db)
	return db, m1, m2
}

func data() (Message, Message) {
	m1 := Message{From: "Evan Pierce", To: []string{"Evan Pierce"}, Subj: "This is a test", Body: "Hel-low!"}
	m2 := Message{From: "Evan B Pierce", To: []string{"Terri Pierce"}, Subj: "This is to my wife", Body: "Hey there sweetie!"}

	return m1, m2
}

func addData(db *sql.DB) (Message, Message) {
	m1, m2 := data()
	Save(db, &m1)
	Save(db, &m2)
	return m1, m2
}

func resetData(assert *assert.Assertions) *sql.DB {
	db := Connect()
	db.Exec(`SELECT reset_message();`)
	return db
}
