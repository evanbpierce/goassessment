# Messaging - Simple messaging database storage

This project is a solution to the problem seen below (Coin Ninja - Take Home Assignment). It is a go package named `messaging`. The go source files in the messaging directory make up the package.

PostgreSQL was chosen for the database engine for this exercise and the pq driver is used (`go get github.com/lib/pq`). The `postgres` folder contains scripts to create the user `evan` (evan.sql), the database itself (database.sql), the `message` table which stores the messages (message.sql) and a function to clear the message table and reset the sequence used by the id (data type serial) field (reset_message.sql).

As required, this package provides tests. These tests can be executed with `go test` while in the messaging directory. The tests use the testify package (`go get github.com/stretchr/testify`), as specified.

The functions available to connect to and disconnect from the database and save, retrieve and delete the messages are as follows:

```Go
func Connect() *sql.DB
    Connects to the database and returns a DB object

func Save(db *sql.DB, m *Message)
    Saves a message to the database message table, creating a new row if necessary and updating the Id field of the message

func Get(db *sql.DB, getid int) Message
    Retrieves a message by its Id value

func Index(db *sql.DB) []Message
    Retrieves all messages in the database

func Delete(db *sql.DB, id int) int64
    Deletes a message by its Id value

func Disconnect(db *sql.DB)
    Disconnects from the database
```

# --- ORIGINAL INSTRUCTIONS ---

# Coin Ninja - Take Home Assignment

This project is intended as a practice exercise to provide us some insight into how well you can Go. Don't worry about showing off all of your abilities, and look to only spend an hour or two for the entirety of this project.

## Problem

We would like to create a simply messaging service. This service should resemble email with a single from address and one-to-many to addresses, as well as a message subject & body.

## Requirements

Implement a struct for a message. This struct should also be able be serialized into JSON using snake case keys (https://en.wikipedia.org/wiki/Snake_case).

Using the `database/sql` package (database of your choosing), create a set of functions to:

- Store a message
- Fetch all stored messages
- Retrieve a single message
- Delete a single message

Include tests where appropriate. When testing use the testify package (https://github.com/stretchr/testify).

Create a readme if appropriate.
