CREATE OR REPLACE FUNCTION reset_message()
RETURNS VOID 
AS $$
BEGIN
--	KILL ALL message ROWS
	DELETE FROM message;

--	RESET THE id (serial) SEQUENCE
	PERFORM SELECT setval('public.message_id_seq', 9223372036854775807, true);

	RETURN;
END
$$ LANGUAGE plpgsql;
