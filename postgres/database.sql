-- Database: coin_ninja

-- DROP DATABASE coin_ninja;

CREATE DATABASE coin_ninja
    WITH 
    OWNER = evan
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_United States.1252'
    LC_CTYPE = 'English_United States.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;