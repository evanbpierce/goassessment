-- Table: public.message

-- DROP TABLE public.message; DROP SEQUENCE public.message_id_seq;

CREATE SEQUENCE public.message_id_seq;

ALTER SEQUENCE public.message_id_seq
    OWNER TO evan;

CREATE TABLE public.message
(
    id integer NOT NULL DEFAULT nextval('message_id_seq'::regclass),
    message json NOT NULL,
    CONSTRAINT message_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.message
    OWNER to evan;